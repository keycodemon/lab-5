﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GradesPrototype.Data;
using GradesPrototype.Services;

namespace GradesPrototype.Controls
{
    /// <summary>
    /// Interaction logic for ChangePasswordDialog.xaml
    /// </summary>
    public partial class ChangePasswordDialog : Window
    {
        public ChangePasswordDialog()
        {
            InitializeComponent();
        }

        // If the user clicks OK to change the password, validate the information that the user has provided
        private void ok_Click(object sender, RoutedEventArgs e)
        {
            // TODO: Exercise 2: Task 4a: Get the details of the current user
            User currentUser = (SessionContext.CurrentStudent == null) ? SessionContext.CurrentTeacher as User : SessionContext.CurrentStudent as User;

            // TODO: Exercise 2: Task 4b: Check that the old password is correct for the current user
            if (!currentUser.VerifyPassword(oldPassword.Password))
            {
                MessageBox.Show("Your old password not correct", "Wrong old password", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            // TODO: Exercise 2: Task 4c: Check that the new password and confirm password fields are the same
            if(!newPassword.Password.Equals(confirm.Password))
            {
                MessageBox.Show("Your new password and confirm password are not match", "New password is not match", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            // TODO: Exercise 2: Task 4d: Attempt to change the password
            // If the password is not sufficiently complex, display an error message
            try
            {
                currentUser.SetPassword(newPassword.Password);
            }catch(ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Error change password", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            // Indicate that the data is valid
            this.DialogResult = true;
        }
    }
}
